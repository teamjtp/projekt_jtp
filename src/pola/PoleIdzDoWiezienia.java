package pola;

import monopoly.Gracz;
import util.Okienka;
import util.Ustawienia;

/**
 * Pole planszy, po wejściu na które gracz trafia do więzienia.
 */
public class PoleIdzDoWiezienia extends PoleGry implements Pole
{
    public PoleIdzDoWiezienia(String nazwa) {
        super(nazwa);
    }

    /**
     * {@inheritDoc}
     * Umieszcza gracza w więzieniu.
     * @param gracz gracz, który wszedł na pole
     */
    @Override
    public void PodejmijCzynnosc(Gracz gracz)
    {
        Okienka.WyswietlWiadomosc("Zostałeś aresztowany. Przejdź prosto do więzienia. " +
                "Nie przechodź przez start. Nie pobieraj " + Ustawienia.kwotaStart + " zł",
                "Idź do więzienia!");
        gracz.idzDoWiezienia();
    }
}
