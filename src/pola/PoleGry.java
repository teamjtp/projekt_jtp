package pola;

/**
 * Klasa przeznaczona do dziedzieczenia pól gry.
 */
public class PoleGry
{
    protected String nazwa;

    public PoleGry(String nazwa) {
        this.nazwa = nazwa;
    }
}
