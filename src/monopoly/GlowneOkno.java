package monopoly;

import pola.Pole;
import pola.PolePlanszy;
import util.Okienka;
import util.Ustawienia;
import wyjatki.BoardCreatingException;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * Główne okno z grą.
 */
public class GlowneOkno extends JFrame {
    private int[] wspolrzedneX = {600, 552, 504, 456, 408, 360, 312, 264, 216, 168, 82, 82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
            168, 216, 264, 312, 360, 408, 456, 504, 552, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600};
    private int[] wspolrzedneY = {600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 552, 504, 456, 408, 360, 312, 264, 216, 168, 100,
            100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 168, 216, 264, 312, 360, 408, 456, 504, 552};

    private PolePlanszy[] planszaGrafika;
    private PanelKarta[] karty;
    private JLabel obecnyGraczLabel;
    private int indeksGracza;
    private Plansza plansza;
    private Gracz obecnyGracz;
    private List<Gracz> listaGraczy;
    private int iloscGraczy;
    private JButton wykonajTureButton;
    private JLabel[] gotowkaGracze;

    public GlowneOkno() throws BoardCreatingException
    {
        indeksGracza = 0;
        iloscGraczy = Ustawienia.iloscGraczy;
        gotowkaGracze = new JLabel[4];
        plansza = new Plansza();
        listaGraczy = new LinkedList<>();
        //utworzenie graczy
        for (int i = 0; i < iloscGraczy; i++)
            listaGraczy.add(new Gracz(Ustawienia.pieniadzePoczatkowe));
        obecnyGracz = listaGraczy.get(indeksGracza);

        //utworzenie jframe
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(1100,800);
        this.setLayout(null);
        this.setMinimumSize(new Dimension(1100,800));
        this.setTitle("Monopol");
        planszaGrafika = new PolePlanszy[40];
        karty = new PanelKarta[40];
        //stworzenie planszy
        for( int i = 0; i < 40; i++)
        {
            final int indeks = i;
            try {
                BufferedImage img = ImageIO.read(new File("img/" + indeks + ".jpg"));
                int height = img.getHeight();
                int width = img.getWidth();
                //stworzenie pola na planszy
                PolePlanszy pole = new PolePlanszy(wspolrzedneX[indeks], wspolrzedneY[indeks], width, height, img);
                planszaGrafika[indeks] = pole;
                //stworzenie karty danego pola
                PanelKarta karta = new PanelKarta(indeks);
                karty[indeks] = karta;
                this.add(karta);
                //dodanie listenera wyswietlajacego karte po najechaniu na pole
                pole.addMouseListener(new MouseListener(){
                    public void mousePressed(MouseEvent e) {}
                    public void mouseReleased(MouseEvent e) {}

                    public void mouseEntered(MouseEvent e) {
                        karty[indeks].setVisible(true);
                    }

                    public void mouseExited(MouseEvent e) {
                        karty[indeks].setVisible(false);
                    }

                    public void mouseClicked(MouseEvent e) {}
                });

                this.add(pole);

            } catch (Exception e) {
                System.out.println("Błąd");
            }
        }
        //stworzenie przycisku od wykonania tury
        wykonajTureButton = new JButton("Wykonaj turę");
        wykonajTureButton.setBounds(800, 552, 150, 40);
        wykonajTureButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    WykonajTure();
                } catch (BoardCreatingException e1) {
                    e1.printStackTrace();
                }
            }
        });
        this.add(wykonajTureButton);

        //stworzenie JLabela odpowiedzialnego za wyswietlenie obecnego gracza
        obecnyGraczLabel = new JLabel();
        obecnyGraczLabel.setFont(new Font("Arial", Font.PLAIN, 22));
        obecnyGraczLabel.setBounds(300, 30, 250, 50);
        this.add(obecnyGraczLabel);

        //stworzenie napisów "gracz x gotowka: y"
        for (int i = 0; i < iloscGraczy; i++)
        {
            gotowkaGracze[i] = new JLabel();
            gotowkaGracze[i].setBounds(750, (100 + (i * 50)), 250, 50);
            gotowkaGracze[i].setFont(new Font("Arial", Font.PLAIN, 18));
            this.add(gotowkaGracze[i]);
        }

        this.UstawGracza();
        this.OdswiezPieniadze();
        //wyswietlenie pionkow graczy na starcie
        for (int i = 0; i < iloscGraczy; i++)
        {
            planszaGrafika[0].wyswietlPionek(i);
        }
    }

    public void UstawGracza()
    {
        obecnyGraczLabel.setText("Obecny gracz: " + (indeksGracza + 1));
    }

    public void OdswiezPieniadze()
    {
        for (int i = 0; i < iloscGraczy; i++)
        {
            gotowkaGracze[i].setText("Graccz " + (i + 1) + " gotówka: " +
                                     listaGraczy.get(i).getIloscPieniedzy() + " zł");
        }
    }

    public void WykonajTure() throws BoardCreatingException
    {
        //ustawienie obecnego gracza
        obecnyGracz = listaGraczy.get(indeksGracza);
        //pocatek tury gracza
        obecnyGracz.startTury();
        //odswiezenie gui
        this.OdswiezPieniadze();
        //wykonanie tury, jezeli gracz nie znajduje sie w wiezieniu
        if(!obecnyGracz.czyWWiezieniu())
        {
            //usuniecie pionka z pola, na ktorym stal gracz
            planszaGrafika[obecnyGracz.getPozycja()].skasujPionek(indeksGracza);
            //wykonanie ruchu przez gracza
            obecnyGracz.wykonajRuch();
            //pobranie indeksu pola, na ktore idzie gracz
            int indeks = obecnyGracz.getPozycja();
            //wyswietlenie pionka danego gracza na nowym polu
            planszaGrafika[indeks].wyswietlPionek(indeksGracza);
            //wykonanie czynnosci na polu, na ktore wszedl gracz
            Pole pole = plansza.getPole(indeks);
            pole.PodejmijCzynnosc(obecnyGracz);
            //odswiezenie gui
            this.OdswiezPieniadze();
            //wykonanie akcji na polu jezeli gracz zostal przemieszczony (np. przez karte Szansa)
            if (indeks != obecnyGracz.getPozycja())
            {
                planszaGrafika[indeks].skasujPionek(indeksGracza);
                planszaGrafika[obecnyGracz.getPozycja()].wyswietlPionek(indeksGracza);
                plansza.getPole(obecnyGracz.getPozycja()).PodejmijCzynnosc(obecnyGracz);
                this.OdswiezPieniadze();
            }
        }
        //wyeliminowanie gracza, jezeli stracil wszystkie pieniadze
        if (obecnyGracz.getIloscPieniedzy() < 0)
        {
            Okienka.WyswietlWiadomosc("Gracz " + (indeksGracza + 1) + " posiada ujemną ilość pieniędzy" +
                    "i wypada z gry.", "Przegrałeś");
            listaGraczy.remove(obecnyGracz);
            if (this.listaGraczy.size() <= 1)
            {
                wykonajTureButton.setEnabled(false);
                Okienka.WyswietlWiadomosc("W grze został tylko jeden gracz, następuje koniec gry",
                        "Koniec gry");
            }
        }
        else
        {
            indeksGracza++;
            indeksGracza %= iloscGraczy;
            UstawGracza();
        }
    }
}
