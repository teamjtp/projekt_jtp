package monopoly;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Mikolaj Mazur on 08.06.2016.
 */
public class PanelKarta extends JPanel
{
    BufferedImage Image;

    public PanelKarta(int i)
    {
        setBounds(300, 270, 160, 212);
        setVisible(false);
        try
        {
            Image = ImageIO.read(new File("img/" + i + ".png"));
        }
        catch(IOException e){
            e.printStackTrace();
        }

    }

    @Override
    public void paintComponent(Graphics g)
    {
        g.drawImage(Image, 0, 0, null);
    }
}
